"""newnstein URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls import (
handler400, handler403, handler404, handler500
)

handler400 = 'home.views.error400'
handler403 = 'home.views.error403'
handler404 = 'home.views.error404'
handler500 = 'home.views.error500'

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'home.views.home', name='home'),
    url(r'^(?P<class_id>\d+)/', include('dashboard.urls')),
    url(r'^dashboard/$', 'dashboard.views.dashboard', name="dashboard"),
    url(r'^accounts/', include('account.urls')),
    url(r'^settings/', include('settings.urls')),
]
