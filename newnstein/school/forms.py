from django import forms
from .models import School

class SchoolCreationForm(forms.ModelForm):
    """A form for creating new teacher. Includes all the required fields"""

    plan = forms.CharField(required=True, max_length=20, widget=forms.HiddenInput(), initial="school_10")
    Number_of_Teachers = forms.IntegerField(required=True, widget=forms.HiddenInput(), initial=0)

    class Meta:
        model = School
        fields = ['city', 'country', 'school','plan', 'Number_of_Teachers']
