from django.db import models
from teacher.models import CommonInfo
# Create your models here.

class School(CommonInfo):
    Number_of_Teachers = models.IntegerField()
    class Meta(CommonInfo.Meta):
        db_table = 'school_school'
