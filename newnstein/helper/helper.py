from teacher.models import Lecture
from quiz.models import Quiz
from exercises.models import Exercise, AvailableExercise

import datetime
from datetime import timedelta

def find_topics(class_id):
    #Iterating through all lecture to find all unique topics
    topics =  []
    for lecture  in Lecture.objects.filter(class_id=class_id):
        new_topic = True
        for topic in topics:
            if(lecture.topic == topic):
                new_topic=False
        if(new_topic):
            topics.append(lecture.topic)
    return topics

def find_categories():
    #Iterating through all lecture to find all unique topics
    categories =  []
    for quiz  in Quiz.objects.all():
        new_category = True
        for category in categories:
            if(quiz.category == category):
                new_category=False
        if(new_category):
            categories.append(quiz.category)
    return categories


def default_start_time():
    now = datetime.datetime.now()
    start = now.replace(hour=0, minute=0, second=0, microsecond=0)
    return start

def time_plus(time, timedelta_calc):
    if not isinstance(timedelta_calc, datetime.timedelta):
        # This is the following constructor format
        # timedelta([days[, seconds[, microseconds[, milliseconds[, minutes[, hours[, weeks]]]]]]])
        timedelta_calc = timedelta(0, timedelta_calc.second, 0, 0, timedelta_calc.minute, timedelta_calc.hour, 0)

    start = datetime.datetime(
        2000, 1, 1,
        hour=time.hour, minute=time.minute, second=time.second)
    end = start + timedelta_calc
    return end.time()

def time_add(time1, time2):
    time_return = datetime.datetime(
        2000, 1, 1,
        hour=(time1.hour + time2.hour), minute=(time1.minute + time2.minute), second=(time1.second + time2.second))
    return time_return

def find_exercises(class_id):
    exercises =  []
    for exercise in Exercise.objects.filter(class_id=class_id):
        e = AvailableExercise.objects.filter(id=exercise.exercise_id)[0]
        exercises.append({'name': e.title, 'url': e.url})
    return exercises
