"""newnstein URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^class/add/$', 'settings.views.add_class', name='add_class'),
    url(r'^class/view', 'settings.views.view_class', name='view_class'),
    url(r'^class/add_students', 'settings.views.add_students_to_class', name='add_students_to_class'),
    url(r'^class/activate/(?P<activation_key>\w+)/$', 'settings.views.activate_class', name='activate_class'),
    url(r'^student/(?P<student_id>\w+)/activate/$', 'settings.views.activate_student', name='activate_student'),
    url(r'^request/$', 'settings.views.student_requests_access', name='student_requests_access'),
    url(r'^email', 'settings.views.email', name='email'),
    url(r'^profile/$', 'settings.views.profile', name='profile')
]
