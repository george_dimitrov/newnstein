from django import forms
from teacher.models import Class

class AddStudentsForm(forms.Form):
    class_id = forms.CharField(required=True, max_length=1000)
    to = forms.CharField(required=True, max_length=1000)

    def __init__(self, user, *args, **kwargs):
        super(AddStudentsForm, self).__init__(*args, **kwargs)
        classes = Class.objects.filter(teacher_email=user)
        self.fields['class_id'] = forms.ChoiceField(choices=[(c.id, str(c.name)) for c in classes])
