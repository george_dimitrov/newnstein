from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.db import IntegrityError
from django.template import loader, Context
from django.core.mail import EmailMessage
from django.conf import settings

from teacher.models import Lecture, Class, Teacher
from exercises.models import Exercise, AvailableExercise
from teacher.forms import ClassForm
from student.models import Class as StudentClass
from quiz.models import Quiz
from account.models import User
from .forms import AddStudentsForm
import re
from django.db.models import Q

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
def add_class(request):
    if request.POST:
        form = ClassForm(request.POST)
        if form.is_valid():
            form.save(request=request)
            messages.success(request, '<strong>Success!</strong> Successfully added the class!')
        else:
            messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again!')

    form = ClassForm(None)
    user = User.objects.filter(email=request.user)[0]
    form.fields["teacher_email"].initial = user.email
    form.fields["teacher_name"].initial = "%s %s" % (user.first_name, user.last_name)

    title = "Add Class"
    context = {
        "title": title,
        "form": form,
        "settings": True
    }
    return render(request, "class/add_class.html", context)

@login_required
def view_class(request):
    title = "View Classes"

    user = User.objects.filter(email=request.user)
    if user[0].user_type == 'TE':
        classes = Class.objects.filter(teacher_email=request.user)
        has_classes = len(classes)
        for i in range(len(classes)):
            classes[i].lectures = Lecture.objects.filter(class_id=classes[i].id)
            classes[i].quizzes = Quiz.objects.filter(class_id=classes[i].id)
            exercises = Exercise.objects.filter(class_id=classes[i].id)
            classes[i].exercises = []
            for e in exercises:
                classes[i].exercises.append(AvailableExercise.objects.filter(id=e.exercise_id).first())

    elif user[0].user_type == 'ST':
        classes = StudentClass.objects.filter(student_email=request.user)
        has_classes = len(classes)
        for i in range(len(classes)):
            classes[i].lectures = Lecture.objects.filter(class_id=classes[i].class_id)
            classes[i].quizzes = Quiz.objects.filter(class_id=classes[i].class_id)
            exercises = Exercise.objects.filter(class_id=classes[i].id)
            classes[i].exercises = []
            for e in exercises:
                classes[i].exercises.append(AvailableExercise.objects.filter(id=e.exercise_id).first())


    context = {
        "title": title,
        "classes": classes,
        "has_classes":has_classes,
        "settings": True
    }
    return render(request, "class/view_classes.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'ST', login_url='/dashboard', )
def student_requests_access(request):
    title = "Find Teacher"
    teachers = None
    if request.GET:
        student_id = request.GET.get('student')
        teacher_id = request.GET.get('teacher')
        student = User.objects.filter(id=student_id).first()
        teacher = User.objects.filter(id=teacher_id).first()
        if student and teacher:
            subject = "Newnstein: Student requesting access"
            t = loader.get_template('email/student_request.html')
            c = Context({'first_name': student.first_name, 'last_name': student.last_name, 'email': student.email})
            html_message = t.render(c)
            from_email = getattr(settings, 'REGISTRATION_DEFAULT_FROM_EMAIL', settings.DEFAULT_FROM_EMAIL)
            msg = EmailMessage(subject, html_message, from_email, [], [teacher.email])
            msg.content_subtype = "html"  # Main content is now text/html
            msg.send()
            messages.success(request, '<strong>Success!</strong> Successfully sent request for access!')
            return redirect('/settings/request/')
        else:
            messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again!')
    if request.POST:
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        all_teachers = User.objects.filter(Q(user_type="TE", last_name=last_name) | Q(user_type="TE", email=email))
        teachers = []
        for t in all_teachers:
            teacher = Teacher.objects.filter(id=t.id).first()
            teachers.append({
                "id": t.id,
                "first_name": t.first_name,
                "last_name": t.last_name,
                "email": t.email,
                "city": teacher.city,
                "school": teacher.school,
            })
    context = {
        "title": title,
        "settings": True,
        "teachers": teachers,
        "user": request.user
    }
    return render(request, "class/request_teacher.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
def add_students_to_class(request):
    title = "Add Students"
    if request.method == 'GET':
        form = AddStudentsForm(request.user)
    else:
        form = AddStudentsForm(request.user, request.POST)
        if form.is_valid():
            to = form.cleaned_data['to']
            class_id = form.cleaned_data['class_id']
            class_ = Class.objects.filter(id=class_id)[0]
            emails = to.split(",")
            bcc = []
            pattern = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'
            for i in range(len(emails)):
                match = re.match(pattern, emails[i])
                if match == None:
                	messages.error(request, '<strong>Error!</strong> &quot;%s&quot; is not a valid email! Please try again!' % emails[i])
                else:
                    bcc.append(emails[i])

            if len(bcc) > 0:
                messages.success(request, '<strong>Success!</strong> Invitations sent!')
                subject = "Newnstein: Add class '%s'" % (class_.name)
                url = "www.newnstein.com/settings/class/activate/%s" % (class_.activation_key)
                t = loader.get_template('email/email.html')
                c = Context({"url":url, "class_name": class_.name, "teacher_name": class_.teacher_name })
                html_message = t.render(c)
                msg = EmailMessage(subject, html_message, 'donotreply@newnstein.com', [], bcc)
                msg.content_subtype = "html"  # Main content is now text/html
                msg.send()
    form = AddStudentsForm(request.user)
    context = {
        "title": title,
        "form": form,
        "settings": True
    }
    return render(request, "class/add_students.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
def activate_student(request, student_id):
    title = "Activate Student"
    if request.GET:
        student_id = request.GET.get('student')
        class_id = request.GET.get('class')
        c = Class.objects.filter(id=class_id).first()
        s = User.objects.filter(id=student_id).first()
        student = StudentClass(class_id=c.id, name=c.name, teacher_name=c.teacher_name, teacher_email=c.teacher_email, student_email=s.email)
        student.save()
        messages.success(request, '<strong>Success!</strong> %s %s successfullly added to %s' % (s.first_name, s.last_name, c.name))
        return redirect('/dashboard')
    classes = Class.objects.filter(teacher_email=request.user)
    student = User.objects.filter(id=student_id).first()
    context = {
        "title": title,
        "classes": classes,
        "student": student,
        "settings": True
    }
    return render(request, "settings/activate_student.html", context)

@login_required
def activate_class(request, activation_key):
    title = "View Classes"
    context = {
                "title":title
                }
    c = Class.objects.filter(activation_key=activation_key)
    if len(c) == 1:
        student = StudentClass(class_id=c[0].id, name=c[0].name, teacher_name=c[0].teacher_name, teacher_email=c[0].teacher_email, student_email=request.user)
        try:
            student.save()
            messages.success(request, '<strong>Success!</strong> Class &quot;%s&quot; added!' % (c[0].name))
        except IntegrityError as e:
            messages.success(request, '<strong>Success!</strong> Class &quot;%s&quot; has already been added!' % (c[0].name))
    else:
        messages.error(request, '<strong>Error!</strong> Invalid activation key!' % (c[0].name))
    return render(request, "class/view_classes.html", context)

def email(request):
    return render(request, "email/email.html")

def profile(request):
    user = User.objects.filter(email=request.user).first()
    teacher = Teacher.objects.filter(user_id=user.id).first()
    title = "%s %s" % (user.first_name, user.last_name)
    context = {
        "title":title,
        "user" : user,
        "teacher": teacher,
        "settings": True
    }
    return render(request, "settings/profile.html", context)
