from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages

from .forms import ExerciseForm

@login_required

def newtonian_orbit(request, class_id):
    context = {
        "title" : "Newtonian Orbit"
    }
    return render(request, "exercises/newtonian_orbit/newtonian_orbit.html", context)

@login_required
def electron_proton(request, class_id):
    context = {
        "title" : "Electrons in Between Plates"
    }
    return render(request, "exercises/electron_proton/electron_proton.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
def add_exercise(request, class_id):
    title = "Add Exercise"
    if request.POST:
        form = ExerciseForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, '<strong>Success!</strong> Successfully added the exercise!')
        else:
            messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again!')

    form = ExerciseForm(request.user)
    context = {
        "title":title,
        "form": form,
    }
    return render(request, "exercises/add_exercise.html", context)
