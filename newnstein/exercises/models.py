from django.db import models

class Exercise(models.Model):
    class_id = models.IntegerField()
    exercise_id = models.IntegerField()

class AvailableExercise(models.Model):
    title = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
