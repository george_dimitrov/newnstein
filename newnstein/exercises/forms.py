import os

from django import forms
from .models import Exercise, AvailableExercise
from teacher.models import Class

class ExerciseForm(forms.ModelForm):
    class Meta:
        model = Exercise
        exclude = []

    def __init__(self, user, *args, **kwargs):
        super(ExerciseForm, self).__init__(*args, **kwargs)
        exercises = AvailableExercise.objects.all()
        classes = Class.objects.filter(teacher_email=user)
        self.fields['exercise_id'] = forms.ChoiceField(choices=[(e.id, str(e.title)) for e in exercises])
        self.fields['class_id'] = forms.ChoiceField(choices=[(c.id, str(c.name)) for c in classes])
