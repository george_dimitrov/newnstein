from django.db import models

class Class(models.Model):
    class_id = models.IntegerField(default=1)
    name = models.CharField(max_length=100)
    student_email = models.EmailField(max_length=255)
    teacher_name = models.CharField(max_length=100)
    teacher_email = models.EmailField(max_length=255)

    class Meta:
        unique_together = (("class_id", "student_email"),)
