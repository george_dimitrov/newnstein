$(document).ready(function() {
  $("#lectureForm").submit(function(event){
    for ( instance in CKEDITOR.instances ) {
      CKEDITOR.instances[instance].updateElement();
    }
    $.ajax({
      type:"POST",
      url: "/dashboard/teacher/lecture/add",
      data: $(this).serialize(),
      success: function (data) {
        alert("Congratulation!\nYour operation was successfull");
      },
      error: function(data) {
        alert("Sorry :(\nSomething went wrong with your request to create a new lecture. Please try again.");
      }
    });
  });
});
