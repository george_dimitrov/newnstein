from django.conf.urls import patterns, url

from .views import CategoriesListView,\
    ViewQuizListByCategory, QuizUserProgressView, QuizMarkingList,\
    QuizMarkingDetail, QuizDetailView, QuizTake


urlpatterns = patterns('',
                        url(r'^$', 'quiz.views.view_all_quizzes', name='view_all_quizzes'),
                        url(r'^add/$', 'quiz.views.create_quiz', name='create_quiz'),
                        url(r'^(?P<quiz_id>\d+)/edit/(?P<question_id>\d+)/$', 'quiz.views.edit_quiz', name='edit_quiz'),
                        url(r'^(?P<quiz_id>\d+)/edit/(?P<question_id>\d+)/true/$', 'quiz.views.update_tf_question', name='update_tf_question'),
                        url(r'^(?P<quiz_id>\d+)/edit/(?P<question_id>\d+)/multiple/$', 'quiz.views.update_mc_question', name='update_mc_question'),
                        url(r'^(?P<quiz_id>\d+)/edit/(?P<question_id>\d+)/essay/$', 'quiz.views.update_essay_question', name='update_essay_question'),
                        url(r'^(?P<quiz_id>\d+)/view/$', 'quiz.views.view_quiz', name='view_quiz'),
                        url(r'^(?P<quiz_id>\d+)/delete/$', 'quiz.views.delete_quiz', name='delete_quiz'),
                        url(r'^(?P<quiz_id>\d+)/add_question/$', 'quiz.views.add_question', name='add_question'),
                        url(r'^(?P<quiz_id>\d+)/add_question/true/$', 'quiz.views.add_tf_question', name='add_tf_question'),
                        url(r'^(?P<quiz_id>\d+)/add_question/multiple/$', 'quiz.views.add_mc_question', name='add_mc_question'),
                        url(r'^(?P<quiz_id>\d+)/add_question/essay/$', 'quiz.views.add_essay_question', name='add_essay_question'),

                        url(regex=r'^category/$', view=CategoriesListView.as_view(), name='quiz_category_list_all'),
                        url(regex=r'^category/(?P<category_name>[\w|\W-]+)/$', view=ViewQuizListByCategory.as_view(), name='quiz_category_list_matching'),
                        url(regex=r'^progress/$', view=QuizUserProgressView.as_view(), name='quiz_progress'),
                        url(regex=r'^marking/$', view=QuizMarkingList.as_view(), name='quiz_marking'),
                        url(regex=r'^marking/(?P<pk>[\d.]+)/$', view=QuizMarkingDetail.as_view(), name='quiz_marking_detail'),
                        url(regex=r'^(?P<slug>\d+)/$', view=QuizDetailView.as_view(), name='quiz_start_page'),
                        url(regex=r'^(?P<quiz_name>[\w-]+)/take/$', view=QuizTake.as_view(), name='quiz_question'),
)
