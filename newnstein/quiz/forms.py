from django import forms
from django.forms.widgets import RadioSelect, Textarea
from quiz.models import Category, Quiz, Progress, Sitting, SubCategory, Question
from multichoice.models import MCQuestion, Answer
from true_false.models import TF_Question
from essay.models import Essay_Question
from django.utils.translation import ugettext_lazy as _
from django.forms import formset_factory

class QuestionForm(forms.Form):
    def __init__(self, question, *args, **kwargs):
        super(QuestionForm, self).__init__(*args, **kwargs)
        choice_list = [x for x in question.get_answers_list()]
        self.fields["answers"] = forms.ChoiceField(choices=choice_list,
                                                   widget=RadioSelect)


class EssayAnswerForm(forms.Form):
    def __init__(self, question, *args, **kwargs):
        super(EssayAnswerForm, self).__init__(*args, **kwargs)
        self.fields["answers"] = forms.CharField(
            widget=Textarea(attrs={'style': 'width:100%'}))

class QuizzesForm(forms.ModelForm):
    """
    below is from
    http://stackoverflow.com/questions/11657682/
    django-admin-interface-using-horizontal-filter-with-
    inline-manytomany-field
    """
    class Meta:
        model = Quiz
        exclude = ['class_id', 'date_added']

    questions = forms.CharField(max_length=20, widget=forms.HiddenInput(), required=False, label=_("Questions"))

    def __init__(self, *args, **kwargs):
        super(QuizzesForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['questions'].initial =\
                self.instance.question_set.all().select_subclasses()

    def save(self, class_id, commit=True):
        quiz = super(QuizzesForm, self).save(commit=False)
        quiz.class_id = class_id
        quiz.save()
        quiz.question_set = self.cleaned_data['questions']

        self.save_m2m()
        return quiz

class TFForm(forms.ModelForm):
    class Meta:
        model = TF_Question
        exclude = []

    def __init__(self, *args, **kwargs):
        super(TFForm, self).__init__(*args, **kwargs)
        if kwargs.get('initial'):
            if kwargs['initial'].get('quiz'):
                quiz_id = kwargs['initial']['quiz']
                self.fields['quiz'].queryset = Quiz.objects.filter(id=quiz_id)

class EssayForm(forms.ModelForm):
    class Meta:
        model = Essay_Question
        exclude = []

    def __init__(self, *args, **kwargs):
        super(EssayForm, self).__init__(*args, **kwargs)
        if kwargs.get('initial'):
            if kwargs['initial'].get('quiz'):
                quiz_id = kwargs['initial']['quiz']
                self.fields['quiz'].queryset = Quiz.objects.filter(id=quiz_id)

class MultForm(forms.ModelForm):
    class Meta:
        model = MCQuestion
        exclude = []

    def __init__(self, *args, **kwargs):
        super(MultForm, self).__init__(*args, **kwargs)
        if kwargs.get('initial'):
            if kwargs['initial'].get('quiz'):
                quiz_id = kwargs['initial']['quiz']
                self.fields['quiz'].queryset = Quiz.objects.filter(id=quiz_id)

class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        exclude = ["question"]

#Creating three answers forms for the question
#In the future we would make the extra parameter dynamic
AnswerFormSet = formset_factory(AnswerForm, extra=3)
