import random

from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView, TemplateView, FormView

from teacher.models import Lecture
from .forms import *
from .models import Quiz, Category, Progress, Sitting, Question
from multichoice.models import MCQuestion, Answer
from essay.models import Essay_Question
from true_false.models import TF_Question

from django.contrib import messages
from dashboard.views import in_class

data = {
     'form-TOTAL_FORMS': '3',
     'form-INITIAL_FORMS': '0',
     'form-MAX_NUM_FORMS': '',
}


class QuizMarkerMixin(object):
    @method_decorator(login_required)
    @method_decorator(user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', ))
    def dispatch(self, *args, **kwargs):
        return super(QuizMarkerMixin, self).dispatch(*args, **kwargs)


class SittingFilterTitleMixin(object):
    def get_queryset(self):
        queryset = super(SittingFilterTitleMixin, self).get_queryset()
        quiz_filter = self.request.GET.get('quiz_filter')
        if quiz_filter:
            queryset = queryset.filter(quiz__title__icontains=quiz_filter)

        return queryset

class QuizDetailView(DetailView):
    model = Quiz
    slug_field = 'id'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if self.object.draft and not request.user.has_perm('quiz.change_quiz'):
            raise PermissionDenied

        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class CategoriesListView(ListView):
    model = Category


class ViewQuizListByCategory(ListView):
    model = Quiz
    template_name = 'view_quiz_category.html'

    def dispatch(self, request, *args, **kwargs):
        self.category = get_object_or_404(
            Category,
            category=self.kwargs['category_name']
        )

        return super(ViewQuizListByCategory, self).\
            dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ViewQuizListByCategory, self)\
            .get_context_data(**kwargs)

        context['category'] = self.category
        return context

    def get_queryset(self):
        queryset = super(ViewQuizListByCategory, self).get_queryset()
        return queryset.filter(category=self.category, draft=False)


class QuizUserProgressView(TemplateView):
    template_name = 'progress.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(QuizUserProgressView, self)\
            .dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(QuizUserProgressView, self).get_context_data(**kwargs)
        progress, c = Progress.objects.get_or_create(user=self.request.user)
        context['cat_scores'] = progress.list_all_cat_scores
        context['exams'] = progress.show_exams()
        return context


class QuizMarkingList(QuizMarkerMixin, SittingFilterTitleMixin, ListView):
    model = Sitting

    def get_queryset(self):
        queryset = super(QuizMarkingList, self).get_queryset()\
                                               .filter(complete=True)

        user_filter = self.request.GET.get('user_filter')
        if user_filter:
            #Right now, we filter by emaul. In the future, we might change this to cpmapre it the username
            queryset = queryset.filter(user__email__icontains=user_filter)

        return queryset


class QuizMarkingDetail(QuizMarkerMixin, DetailView):
    model = Sitting

    def post(self, request, *args, **kwargs):
        sitting = self.get_object()

        q_to_toggle = request.POST.get('qid', None)
        if q_to_toggle:
            q = Question.objects.get_subclass(id=int(q_to_toggle))
            if int(q_to_toggle) in sitting.get_incorrect_questions:
                sitting.remove_incorrect_question(q)
            else:
                sitting.add_incorrect_question(q)

        return self.get(request)

    def get_context_data(self, **kwargs):
        context = super(QuizMarkingDetail, self).get_context_data(**kwargs)
        context['questions'] =\
            context['sitting'].get_questions(with_answers=True)
        return context


class QuizTake(FormView):
    form_class = QuestionForm
    template_name = 'question.html'

    def dispatch(self, request, *args, **kwargs):
        self.quiz = get_object_or_404(Quiz, id=self.kwargs['quiz_name'])
        if self.quiz.draft and not request.user.has_perm('quiz.change_quiz'):
            raise PermissionDenied

        self.logged_in_user = self.request.user.is_authenticated()

        if self.logged_in_user:
            self.sitting = Sitting.objects.user_sitting(request.user,
                                                        self.quiz)
        else:
            self.sitting = self.anon_load_sitting()

        if self.sitting is False:
            return render(request, 'single_complete.html')

        return super(QuizTake, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class):
        if self.logged_in_user:
            self.question = self.sitting.get_first_question()
            self.progress = self.sitting.progress()
        else:
            self.question = self.anon_next_question()
            self.progress = self.anon_sitting_progress()

        if self.question.__class__ is Essay_Question:
            form_class = EssayAnswerForm

        return form_class(**self.get_form_kwargs())

    def get_form_kwargs(self):
        kwargs = super(QuizTake, self).get_form_kwargs()

        return dict(kwargs, question=self.question)

    def form_valid(self, form):
        if self.logged_in_user:
            self.form_valid_user(form)
            if self.sitting.get_first_question() is False:
                return self.final_result_user()
        else:
            self.form_valid_anon(form)
            if not self.request.session[self.quiz.anon_q_list()]:
                return self.final_result_anon()

        self.request.POST = {}

        return super(QuizTake, self).get(self, self.request)

    def get_context_data(self, **kwargs):
        context = super(QuizTake, self).get_context_data(**kwargs)
        context['question'] = self.question
        context['quiz'] = self.quiz
        if hasattr(self, 'previous'):
            context['previous'] = self.previous
        if hasattr(self, 'progress'):
            context['progress'] = self.progress
        return context

    def form_valid_user(self, form):
        progress, c = Progress.objects.get_or_create(user=self.request.user)
        guess = form.cleaned_data['answers']
        is_correct = self.question.check_if_correct(guess)

        if is_correct is True:
            self.sitting.add_to_score(1)
            progress.update_score(self.question, 1, 1)
        else:
            self.sitting.add_incorrect_question(self.question)
            progress.update_score(self.question, 0, 1)

        if self.quiz.answers_at_end is not True:
            self.previous = {'previous_answer': guess,
                             'previous_outcome': is_correct,
                             'previous_question': self.question,
                             'answers': self.question.get_answers(),
                             'question_type': {self.question
                                               .__class__.__name__: True}}
        else:
            self.previous = {}

        self.sitting.add_user_answer(self.question, guess)
        self.sitting.remove_first_question()

    def final_result_user(self):
        results = {
            'quiz': self.quiz,
            'score': self.sitting.get_current_score,
            'max_score': self.sitting.get_max_score,
            'percent': self.sitting.get_percent_correct,
            'sitting': self.sitting,
            'previous': self.previous,
        }

        self.sitting.mark_quiz_complete()

        if self.quiz.answers_at_end:
            results['questions'] =\
                self.sitting.get_questions(with_answers=True)
            results['incorrect_questions'] =\
                self.sitting.get_incorrect_questions

        if self.quiz.exam_paper is False:
            self.sitting.delete()

        return render(self.request, 'result.html', results)

    def anon_load_sitting(self):
        if self.quiz.single_attempt is True:
            return False

        if self.quiz.anon_q_list() in self.request.session:
            return self.request.session[self.quiz.anon_q_list()]
        else:
            return self.new_anon_quiz_session()

    def new_anon_quiz_session(self):
        """
        Sets the session variables when starting a quiz for the first time
        as a non signed-in user
        """
        self.request.session.set_expiry(259200)  # expires after 3 days
        questions = self.quiz.get_questions()
        question_list = [question.id for question in questions]

        if self.quiz.random_order is True:
            random.shuffle(question_list)

        if self.quiz.max_questions and (self.quiz.max_questions
                                        < len(question_list)):
            question_list = question_list[:self.quiz.max_questions]

        # session score for anon users
        self.request.session[self.quiz.anon_score_id()] = 0

        # session list of questions
        self.request.session[self.quiz.anon_q_list()] = question_list

        # session list of question order and incorrect questions
        self.request.session[self.quiz.anon_q_data()] = dict(
            incorrect_questions=[],
            order=question_list,
        )

        return self.request.session[self.quiz.anon_q_list()]

    def anon_next_question(self):
        next_question_id = self.request.session[self.quiz.anon_q_list()][0]
        return Question.objects.get_subclass(id=next_question_id)

    def anon_sitting_progress(self):
        total = len(self.request.session[self.quiz.anon_q_data()]['order'])
        answered = total - len(self.request.session[self.quiz.anon_q_list()])
        return (answered, total)

    def form_valid_anon(self, form):
        guess = form.cleaned_data['answers']
        is_correct = self.question.check_if_correct(guess)

        if is_correct:
            self.request.session[self.quiz.anon_score_id()] += 1
            anon_session_score(self.request.session, 1, 1)
        else:
            anon_session_score(self.request.session, 0, 1)
            self.request\
                .session[self.quiz.anon_q_data()]['incorrect_questions']\
                .append(self.question.id)

        self.previous = {}
        if self.quiz.answers_at_end is not True:
            self.previous = {'previous_answer': guess,
                             'previous_outcome': is_correct,
                             'previous_question': self.question,
                             'answers': self.question.get_answers(),
                             'question_type': {self.question
                                               .__class__.__name__: True}}

        self.request.session[self.quiz.anon_q_list()] =\
            self.request.session[self.quiz.anon_q_list()][1:]

    def final_result_anon(self):
        score = self.request.session[self.quiz.anon_score_id()]
        q_order = self.request.session[self.quiz.anon_q_data()]['order']
        max_score = len(q_order)
        percent = int(round((float(score) / max_score) * 100))
        session, session_possible = anon_session_score(self.request.session)
        if score is 0:
            score = "0"

        results = {
            'score': score,
            'max_score': max_score,
            'percent': percent,
            'session': session,
            'possible': session_possible
        }

        del self.request.session[self.quiz.anon_q_list()]

        if self.quiz.answers_at_end:
            results['questions'] = sorted(
                self.quiz.question_set.filter(id__in=q_order)
                                      .select_subclasses(),
                key=lambda q: q_order.index(q.id))

            results['incorrect_questions'] = (
                self.request
                    .session[self.quiz.anon_q_data()]['incorrect_questions'])

        else:
            results['previous'] = self.previous

        del self.request.session[self.quiz.anon_q_data()]

        return render(self.request, 'result.html', results)


def anon_session_score(session, to_add=0, possible=0):
    """
    Returns the session score for non-signed in users.
    If number passed in then add this to the running total and
    return session score.

    examples:
        anon_session_score(1, 1) will add 1 out of a possible 1
        anon_session_score(0, 2) will add 0 out of a possible 2
        x, y = anon_session_score() will return the session score
                                    without modification

    Left this as an individual function for unit testing
    """
    if "session_score" not in session:
        session["session_score"], session["session_score_possible"] = 0, 0

    if possible > 0:
        session["session_score"] += to_add
        session["session_score_possible"] += possible

    return session["session_score"], session["session_score_possible"]

#Deals with the creation of the initial quizzes
#If a quiz form is submitted and its valid, we will initialize the forms for questions
#and select the just created quiz in the dropdown menu
@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def create_quiz(request, class_id):
    form = QuizzesForm(request.POST or None)
    class_lectures = Lecture.objects.filter(class_id=class_id)
    if request.POST:
        if form.is_valid():
            quiz = form.save(class_id)
            return redirect('/%s/quizzes/%s/add_question' % (class_id, quiz.pk))
        else:
            messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    context = {
        "form" : form,
        "title": "Create Quiz",
        "class_lectures": class_lectures
    }
    return render(request, "quizzes/add_quiz.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def add_question(request, class_id, quiz_id):
    quiz = Quiz.objects.filter(id=quiz_id).first()
    form_t = TFForm(initial={'quiz': quiz_id})
    form_e = EssayForm(initial={'quiz': quiz_id})
    form_q = MultForm(initial={'quiz': quiz_id})
    form_a = AnswerFormSet()
    title = 'Add Questions'
    context = {
        "form_t": form_t,
        "form_e": form_e,
        "form_q": form_q,
        "form_a": form_a,
        "quiz_id": quiz_id,
        "title": title,
        "quiz": quiz
    }
    return render(request, "quizzes/add_question.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def add_tf_question(request, class_id, quiz_id):
    if request.POST:
        form_t = TFForm(request.POST)
        print request.POST
        if form_t.is_valid():
            form_t.save()
            messages.success(request, '<strong>Success!</strong> Successfully saved True/False question!')

    else:
        messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    return redirect('/%s/quizzes/%s/add_question' % (class_id, quiz_id))

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def add_mc_question(request, class_id, quiz_id):
    if request.POST:
        form_q = MultForm(request.POST)
        if form_q.is_valid():
            question = form_q.save()
            num = 1
            while request.POST.get('content-%s' % (num)):

                content = request.POST.get('content-%s' % (num))
                if request.POST.get('correct-%s' % (num)):
                    correct = True
                else:
                    correct = False
                print question.pk, content, correct
                answer_input = Answer(question=question, content=content, correct=correct)
                answer_input.save()
                num += 1
            messages.success(request, '<strong>Success!</strong> Successfully saved Multiple Choice question!')
        else:
            messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    else:
        messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    return redirect('/%s/quizzes/%s/add_question' % (class_id, quiz_id))

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def add_essay_question(request, class_id, quiz_id):
    if request.POST:
        form_e = EssayForm(request.POST)
        if form_e.is_valid():
            form_e.save()
            messages.success(request, '<strong>Success!</strong> Successfully saved Essay question!')
    else:
        messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    return redirect('/%s/quizzes/%s/add_question' % (class_id, quiz_id))

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def update_tf_question(request, class_id, quiz_id, question_id):
    if request.POST:
        form_t = TFForm(request.POST)
        if form_t.is_valid():
            question = form_t.save(commit=False)
            question.id = question_id
            question.save()
            messages.success(request, '<strong>Success!</strong> Successfully saved True/False question!')

    else:
        messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    return redirect('/%s/quizzes/%s/view' % (class_id, quiz_id))

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def update_mc_question(request, class_id, quiz_id, question_id):
    if request.POST:
        form_q = MultForm(request.POST)
        if form_q.is_valid():
            question = form_q.save(commit=False)
            question.id = question_id
            question.save()
            num = 1
            while request.POST.get('content-%s' % (num)):

                content = request.POST.get('content-%s' % (num))
                answer_id = request.POST.get('answer-%s' % (num))
                if request.POST.get('correct-%s' % (num)):
                    correct = True
                else:
                    correct = False
                answer_input = Answer(question=question, content=content, correct=correct)
                Answer.objects.filter(question=question, id=answer_id).update(content=content, correct=correct)
                num += 1
            messages.success(request, '<strong>Success!</strong> Successfully saved Multiple Choice question!')
        else:
            messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    else:
        messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    return redirect('/%s/quizzes/%s/view' % (class_id, quiz_id))

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def update_essay_question(request, class_id, quiz_id, question_id):
    if request.POST:
        form_e = EssayForm(request.POST)
        print 'here2'
        if form_e.is_valid():
            print form_e
            question = form_e.save(commit=False)
            question.id = question_id
            question.save()
            messages.success(request, '<strong>Success!</strong> Successfully saved Essay question!')

        print form_e.is_valid()
    else:
        messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    return redirect('/%s/quizzes/%s/view' % (class_id, quiz_id))

def edit_quiz (request, class_id, quiz_id, question_id):
    form_t = form_q = form_e = None
    quiz = Quiz.objects.filter(id = quiz_id).first()
    question = Question.objects.filter(quiz__id=quiz_id, id=question_id).first()
    tfanswers = TF_Question.objects.filter(question_ptr_id=question_id).first()
    mcanswers = Answer.objects.filter(question=question_id)
    print tfanswers, mcanswers
    if tfanswers:
        form_t = TFForm(initial={'content': question.content, 'explanation': question.explanation, 'correct': tfanswers.correct, 'quiz': quiz_id})
    elif mcanswers:
        form_q = MultForm(initial={'content': question.content, 'explanation': question.explanation, 'quiz': quiz_id})
    else:
        print 'here'
        form_e = EssayForm(initial={'content': question.content, 'explanation': question.explanation, 'quiz': quiz_id})

    context = {
        "class_id": class_id,
        "quiz_id": quiz_id,
        "question_id": question_id,
        "quiz":quiz,
        "question":question,
        "form_t": form_t,
        "form_q": form_q,
        "mcanswers": mcanswers,
        "form_e": form_e
    }
    return render(request, "quizzes/quiz_edit.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def view_quiz(request, class_id, quiz_id):
    quiz = Quiz.objects.filter(id = quiz_id).first()
    questions = Question.objects.filter(quiz__id=quiz_id)
    quiz_content = []
    question_number = 1
    for q in questions:
        quiz_content.append({
            "question_id": q.id,
            "number": question_number,
            "question":q.content,
            "mcanswers":Answer.objects.filter(question=q.id),
            "tfanswers":TF_Question.objects.filter(question_ptr_id=q.id).first(),
            "explanation":q.explanation
        })
        question_number += 1

    context = {
        "quiz": quiz,
        "quiz_content": quiz_content,
        "quiz_id": quiz_id
    }
    return render(request, "quizzes/quiz_view.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
def delete_quiz(request, class_id, quiz_id):
    try:
        quiz = Quiz.objects.filter(id = quiz_id).delete()
        messages.success(request, '<strong>Success!</strong> Quiz was successfully deleted!')
    except:
        messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again')
    return redirect('/dashboard')

@login_required
@in_class
def view_all_quizzes(request, class_id):
    user = request.user
    quizzes = Quiz.objects.filter(class_id=class_id)
    title = 'View Quizzes'
    context = {
        'quizzes': quizzes,
        'user_type': user.user_type,
        'title': title
    }
    return render(request, "quizzes/quiz_list.html", context)
