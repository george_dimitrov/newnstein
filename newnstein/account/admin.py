from django.contrib import admin
from .forms import UserAdmin
from .models import User
from django.contrib.auth.models import Group
# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)
# Register your models here.
