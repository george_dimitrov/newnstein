from django.shortcuts import redirect, render
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.sites.shortcuts import get_current_site
from django.core.urlresolvers import reverse
from django.forms.forms import NON_FIELD_ERRORS
from django.conf import settings
from django.contrib import messages

from .forms import UserCreationForm
from teacher.forms import TeacherCreationForm
from school.forms import SchoolCreationForm
from teacher.models import Teacher
from school.models import School
from .models import User

import stripe

def register(request):
    response = None
    form = UserCreationForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            user_type = form['user_type'].value()
            form.save(request=request)
            if user_type == "ST":
                messages.success(request, 'Activation email has been sent')
            else:
                request.session['email'] = form['email'].value()
                request.session['password'] = form['password1'].value()
                if user_type == "TE":
                    return redirect('/accounts/register/teacher')
                else:
                    return redirect('/accounts/register/school')
        else:
            messages.error(request, '<strong>Failure!</strong> Something went wrong! Please make sure all of the required fields are filled')
    context = {
        "title": "Register",
        "form": form,
        "response": response
    }
    return render(request, "registration/registration_form.html", context)

def register_school(request, type=None):
    if 'email' not in request.session:
        return redirect('/accounts/register')

    email = request.session['email']
    password = request.session['password']
    if type == 'teacher':
        school = False
        teacher = True
        form = TeacherCreationForm(request.POST or None)
    else:
        school = True
        teacher = False
        form = SchoolCreationForm(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            user = User.objects.filter(email=email)
            city = form.cleaned_data['city']
            country = form.cleaned_data['country']
            school = form.cleaned_data['school']
            plan = form.cleaned_data['plan']
            if teacher ==  True:
                teacher = Teacher(city=city, country=country, school=school, plan=plan, user_id=user[0].id)
                teacher.save()
            else:
                Number_of_Teachers=form.cleaned_data['Number_of_Teachers']
                teacher = School(city=city, country=country, school=school, plan=plan, user_id=user[0].id, Number_of_Teachers=Number_of_Teachers)
                teacher.save()
            fee = settings.SUBSCRIPTION_PRICE[plan]
            try:
                stripe_customer = teacher.charge(request, email, fee)
            except stripe.StripeError as e:
                messages.error(request, '<strong>Failure!</strong> Something went wrong! Please make sure all of the required fields are filled')
                form._errors[NON_FIELD_ERRORS] = form.error_class([e.args[0]])
                return render(request, "registration/registration_form.html",
                    {'form':form,
                     'STRIPE_PUBLISHABLE_KEY':settings.STRIPE_PUBLISHABLE_KEY}
                )
            user.update(is_active=True)
            a_u = authenticate(email=email, password=password)
            if a_u is not None:
                if a_u.is_active:
                    login(request, a_u)
                    return HttpResponseRedirect(reverse('dashboard'))
                else:
                    return HttpResponseRedirect(
                        reverse('auth_login')
                    )
            else:
                messages.warning(request, '<strong>Warning!</strong> It appears that you have not completed the registration')
                return redirect('/accounts/register/')
    context = {
        "title": "Register",
        "form": form,
        "school":school,
        "teacher": teacher,
        'STRIPE_PUBLISHABLE_KEY':settings.STRIPE_PUBLISHABLE_KEY,
    }
    return render(request, "registration/registration_form.html", context)

def activate(request, activation_key):
        activated_user = (User.objects.activate_user(activation_key))
        if activated_user:
            messages.success(request, 'Activation successful')
            return redirect('/accounts/login')
        elif not User.objects.filter(activation_key=activation_key).exists():
            messages.error(request, 'Activation key does not exist.')
            return redirect('/accounts/register')
        else:
            messages.warning(request, 'Activation key has expired.')
            return render(request, "registration/activation_expired.html", {"title":"Activation Expired", "activation_key":activation_key})

def activate_expired(request, activation_key):
    user = (User.objects.resend_activation(activation_key))
    if user:
        site = get_current_site(request)
        user.send_activation_email(site, request)
        messages.info(request, 'Activation email sent.')
        return redirect('/accounts/login')
    else:
        messages.error(request, 'Activation key does not exist.')
        return redirect('/accounts/register')
