from django.db import models
import hashlib
import random
import re
from django.conf import settings
import datetime
from django.utils import six
from django.utils.translation import ugettext_lazy as _
from django.core.mail import EmailMultiAlternatives
from django.template import RequestContext, TemplateDoesNotExist
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.utils.timezone import now as datetime_now
from django.template.loader import render_to_string

SHA1_RE = re.compile('^[a-f0-9]{40}$')
USER_TYPE_CHOICES = (
    ('ST', 'Student'),
    ('TE', 'Teacher'),
    ('SC', 'School'),
)

class UserManager(BaseUserManager):
    def create_user(self, email, date_of_birth, password=None):
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            date_of_birth=date_of_birth,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, date_of_birth, password):
        user = self.create_user(email,
            password=password,
            date_of_birth=date_of_birth
        )
        user.is_admin = True
        user.is_active = True
        user.save(using=self._db)
        return user

    def resend_activation(self, activation_key):
        if SHA1_RE.search(activation_key):
            try:
                profile = self.get(activation_key=activation_key)
            except self.model.DoesNotExist:
                # This is an actual activation failure as the activation
                # key does not exist. It is *not* the scenario where an
                # already activated User reuses an activation key.
                return False

            profile.date_joined = datetime.datetime.now()
            profile.save()
            profile.create_new_activation_key()
            return profile
        return False

    def activate_user(self, activation_key, get_profile=False):
        if SHA1_RE.search(activation_key):
            try:
                profile = self.get(activation_key=activation_key)
            except self.model.DoesNotExist:
                return False

            if profile.is_active:
                if profile.is_active:
                    return profile
                else:
                    return False

            if not profile.activation_key_expired():
                profile.is_active = True
                profile.save()
                return profile
        return False

class User(AbstractBaseUser):
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True, )
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    date_of_birth = models.DateField()
    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    user_type = models.CharField(max_length=2, choices=USER_TYPE_CHOICES, default='ST')
    activation_key = models.CharField(_('activation key'), max_length=40)
    date_joined = models.DateTimeField(default=datetime.datetime.now, blank=True)
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['date_of_birth']

    def get_full_name(self):
        return "%s %s" % (self.first_name, self.last_name)

    def get_short_name(self):
        return self.first_name

    def get_user_type(self):
        return self.user_type

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def create_new_activation_key(self, save=True):
        salt = hashlib.sha1(six.text_type(random.random())
                            .encode('ascii')).hexdigest()[:5]
        salt = salt.encode('ascii')
        user_email = str(self.email)
        if isinstance(user_email, six.text_type):
            user_email = user_email.encode('utf-8')
        self.activation_key = hashlib.sha1(salt + user_email).hexdigest()
        if save:
            self.save()
        return self.activation_key

    def activation_key_expired(self):
        expiration_date = datetime.timedelta(
            days=settings.ACCOUNT_ACTIVATION_DAYS)
        return (self.is_active or
                (self.date_joined + expiration_date <= datetime_now()))
    activation_key_expired.boolean = True

    def send_activation_email(self, site, request=None):
        activation_email_body = getattr(settings, 'ACTIVATION_EMAIL_BODY',
                                        'registration/activation_email.txt')
        activation_email_html = getattr(settings, 'ACTIVATION_EMAIL_HTML',
                                        'registration/activation_email.html')

        ctx_dict = {}
        if request is not None:
            ctx_dict = RequestContext(request, ctx_dict)

        ctx_dict.update({
            'user': self,
            'activation_key': self.activation_key,
            'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
            'site': site,
        })
        # Email subject *must not* contain newlines
        subject = 'Welcome to Newnstein'
        from_email = getattr(settings, 'REGISTRATION_DEFAULT_FROM_EMAIL',
                             settings.DEFAULT_FROM_EMAIL)
        message_txt = render_to_string(activation_email_body,
                                       ctx_dict)

        email_message = EmailMultiAlternatives(subject, message_txt,
                                               from_email, [self.email])

        if getattr(settings, 'REGISTRATION_EMAIL_HTML', True):
            try:
                message_html = render_to_string(
                    activation_email_html, ctx_dict)
            except TemplateDoesNotExist:
                pass
            else:
                email_message.attach_alternative(message_html, 'text/html')

        email_message.send()

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.is_admin
