from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.core.urlresolvers import reverse_lazy
from distutils.version import LooseVersion
from django import get_version

urlpatterns = [
    url(r'^login/$', 'dashboard.views.custom_login', name='auth_login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'home/home.html'}, name='auth_logout'),
    url(r'^password/change/$', auth_views.password_change,{'post_change_redirect': reverse_lazy('auth_password_change_done')}, name='auth_password_change'),
    url(r'^password/change/done/$', auth_views.password_change_done, name='auth_password_change_done'),
    url(r'^activate/expired/(?P<activation_key>\w+)/$', 'account.views.activate_expired', name='activate_expired'),
    url(r'^password/reset/$', auth_views.password_reset, {'post_reset_redirect': reverse_lazy('auth_password_reset_done')}, name='auth_password_reset'),
    url(r'^password/reset/complete/$', auth_views.password_reset_complete, name='auth_password_reset_complete'),
    url(r'^password/reset/done/$', auth_views.password_reset_done, name='auth_password_reset_done'),
    url(r'^register/$', 'account.views.register', name='registration_register'),
    url(r'^register/(?P<type>\w+)/$', 'account.views.register_school', name='account_register_other'),
    url(r'^activate/(?P<activation_key>\w+)/$', 'account.views.activate', name='registration_activate'),
]

if (LooseVersion(get_version()) >= LooseVersion('1.6')):
    urlpatterns += [
        url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
            auth_views.password_reset_confirm,
            {'post_reset_redirect': reverse_lazy('auth_password_reset_complete')},
            name='auth_password_reset_confirm')
    ]
else:
    urlpatterns += [
        url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            auth_views.password_reset_confirm,
            {'post_reset_redirect': reverse_lazy('auth_password_reset_complete')},
            name='auth_password_reset_confirm')
    ]
