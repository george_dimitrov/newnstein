from django.shortcuts import render, get_object_or_404, redirect
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.mail import EmailMessage
from django.template import loader, Context

from dashboard.models import WebRequest
from .models import Lecture, Class, UserPerLecture, UserPerQuiz, UserPerExercise
from quiz.models import Quiz, Sitting
from exercises.models import AvailableExercise
from .forms import LectureForm, ClassForm
from account.models import User
from helper.helper import find_topics
from dashboard.views import in_class
import re

#Data responsible for populating the fromset
#This is usedd as the amangment data
#please refer to this link for more information: https://docs.djangoproject.com/en/dev/topics/forms/formsets/#understanding-the-managementform

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def add_lecture(request, class_id, id=None):
    if id:
        lecture = get_object_or_404(Lecture, pk=id)
        form = LectureForm(request.user, request.POST or None, request.FILES or None, instance=lecture)
        title = "Edit Lecture"
    else:
        form = LectureForm(request.user, request.POST or None, request.FILES or None)
        title = "Add Lecture"

    if request.POST:
        if form.is_valid():
            form.save()
            messages.success(request, '<strong>Success!</strong> Successfully added the lecture!')
        else:
            messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again!')

    context = {
        "lecture_data" :  Lecture.objects.filter(id=id),
        "title": title,
        "form": form,
    }
    return render(request, "lecture/add_lecture.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def delete_lecture(request, id, class_id):
    try:
        lecture_obj = Lecture.objects.filter(id=id)
        request_obj = WebRequest.objects.filter(lecture_id = id)
        request_obj.delete()
        lecture_obj[0].delete()
        messages.success(request, '<strong>Success!</strong> Successfully deleted the lecture!')
    except Exception as e:
        print '%s (%s)' % (e.message, type(e))
        print e
        messages.error(request, '<strong>Error!</strong> Something went wrong! Please try again!')
    return redirect('/dashboard/')

#Django removed the autodelete of file fields.
#I created the function to lsten to delete signal and manually delete the file.
#Just to be safe, listening to the post_delete and not pre_delete
@receiver(post_delete, sender=Lecture)
def photo_post_delete_handler(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.attachments.delete(False)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def statistics_lecture(request, class_id):
    title = "Statistics"
    time_for_class_lecture = UserPerLecture.objects.filter(class_id = class_id)
    context = {
        "title": title,
        "lecture_time" : time_for_class_lecture,
    }
    return render(request, "stats/lecture_stats.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def statistics_quiz(request, class_id):
    title = "Statistics"
    time_for_class_quiz = UserPerQuiz.objects.filter(class_id = class_id)
    context = {
        "title": title,
        "quiz_time" : time_for_class_quiz,
    }
    return render(request, "stats/quiz_stats.html", context)

@login_required
@user_passes_test(lambda u: u.user_type == 'TE', login_url='/dashboard', )
@in_class
def statistics_exercise(request, class_id):
    title = "Statistics"
    time_for_class_exercise = UserPerExercise.objects.filter(class_id = class_id)
    context = {
        "title": title,
        "exercise_time" : time_for_class_exercise,
        "available_exercises" : AvailableExercise.objects.filter(),
    }
    return render(request, "stats/exercise_stats.html", context)
