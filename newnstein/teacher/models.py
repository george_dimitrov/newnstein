from django.db import models
from django.conf import settings
from account.models import User
from quiz.models import Quiz
from exercises.models import AvailableExercise, Exercise

import datetime
import stripe
import os
import hashlib
import random
import re

from django.utils import six

class CommonInfo(models.Model):
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    school = models.CharField(max_length=100)
    plan = models.CharField(max_length=20)
    user_id = models.IntegerField()
    stripe_id = models.CharField(max_length=30, blank=True)

    class Meta:
        abstract = True

    def location(self):
        return "%s, %s" % (self.city, self.country)

    def charge(self, request, email, fee):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        token = request.POST['stripeToken']

        stripe_customer = stripe.Customer.create(
            card=token,
            description=email
        )

        self.stripe_id = stripe_customer.id
        self.save()

        stripe.Charge.create(
            amount=fee, # in cents
            currency="cad",
            customer=stripe_customer.id
        )

        return stripe_customer

class Teacher(CommonInfo):
    pass

class Lecture(models.Model):
    topic = models.CharField(max_length=100)
    description = models.CharField(max_length=300, default="No description")
    title = models.CharField(max_length=100, unique=True)
    content = models.TextField()
    attachments = models.FileField(default=None, blank=True, upload_to="attachments/")
    date_added = models.DateTimeField(default=datetime.datetime.now, blank=True)
    class_id = models.IntegerField(default=1)

class UserPerLecture(models.Model):
    user_id = models.ForeignKey(User)
    duration = models.TimeField()
    visits = models.IntegerField(default=0,blank=True, null=True)
    lecture_id = models.ForeignKey(Lecture, blank=True, null=True)
    class_id = models.IntegerField(blank=True, null=True)

class UserPerQuiz(models.Model):
    user_id = models.ForeignKey(User)
    duration = models.TimeField()
    visits = models.IntegerField(default=0)
    quiz_id = models.ForeignKey(Quiz, blank=True, null=True)
    class_id = models.IntegerField(blank=True, null=True)

class UserPerExercise(models.Model):
    user_id = models.ForeignKey(User)
    duration = models.TimeField()
    visits = models.IntegerField(default=0, blank=True, null=True)
    exercise_id = models.ForeignKey(Exercise, blank=True, null=True)
    class_id = models.IntegerField(blank=True, null=True)

class StatsPerTopic(models.Model):
    topic = models.CharField(max_length=100)
    duration = models.TimeField()
    visits = models.IntegerField(default=0, blank=True, null=True)
    class_id = models.IntegerField(default=1)

class StatsPerLecture(models.Model):
    lecture_id = models.ForeignKey(Lecture, blank=True, null=True)
    duration = models.TimeField()
    visits = models.IntegerField(default=0, blank=True, null=True)
    class_id = models.IntegerField(default=1)

class StatsPerQuiz(models.Model):
    quiz_id = models.ForeignKey(Quiz, blank=True, null=True)
    duration = models.TimeField()
    visits = models.IntegerField(default=0, blank=True, null=True)

class Class(models.Model):
    name = models.CharField(max_length=100)
    teacher_name = models.CharField(max_length=100)
    teacher_email = models.EmailField(max_length=255)
    description = models.CharField(max_length=400)
    activation_key = models.CharField(max_length=40, null=True)

    def create_new_activation_key(self, save=True):
        salt = hashlib.sha1(six.text_type(random.random())
                            .encode('ascii')).hexdigest()[:5]
        salt = salt.encode('ascii')
        class_id = str(self.id)
        if isinstance(class_id, six.text_type):
            class_id = class_id.encode('utf-8')
        self.activation_key = hashlib.sha1(salt + class_id).hexdigest()
        if save:
            self.save()
        return self.activation_key
