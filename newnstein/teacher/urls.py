"""newnstein URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""

from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^lecture/add', 'teacher.views.add_lecture', name='add_lecture'),
    url(r'^lecture/edit/(?P<id>\d+)/$', 'teacher.views.add_lecture', name='edit_lecture'),
    url(r'^lecture/delete/(?P<id>\d+)/$', 'teacher.views.delete_lecture', name='delete_lecture'),
    url(r'^statistics/lecture', 'teacher.views.statistics_lecture', name='statistics_lecture'),
    url(r'^statistics/quiz', 'teacher.views.statistics_quiz', name='statistics_quiz'),
    url(r'^statistics/exercise', 'teacher.views.statistics_exercise', name='statistics_exercise'),
    url(r'^quizzes/', include('quiz.urls')),
]
