import os

from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.forms import formset_factory

from .models import Lecture, Teacher, Class
from quiz.models import Category, Quiz, Progress, Sitting, SubCategory, Question
from multichoice.models import MCQuestion, Answer
from true_false.models import TF_Question
from essay.models import Essay_Question
from account.models import User

class ClassForm(forms.ModelForm):
    class Meta:
        model = Class
        fields = ['name', 'description', 'teacher_name', 'teacher_email', ]

    def save(self, request, commit=True):
        save_class = super(ClassForm, self).save(commit=False)
        save_class.create_new_activation_key()
        save_class.save()
        return save_class

class LectureForm(forms.ModelForm):
    class Meta:
        model = Lecture
        fields = ['class_id', 'topic', 'description', 'title','content', 'attachments']

    def __init__(self, user, *args, **kwargs):
        super(LectureForm, self).__init__(*args, **kwargs)
        classes = Class.objects.filter(teacher_email=user)
        self.fields['class_id'] = forms.ChoiceField(choices=[(c.id, str(c.name)) for c in classes])

    def clean_attachments(self):
        attachments = self.cleaned_data.get("attachments")
        if attachments:
            attachments_one = os.path.splitext(attachments.name)[1]
            valid_extensions = ['.pdf','.doc','.docx', '.ppt']
            if not attachments_one in valid_extensions:
                raise forms.ValidationError(u'File not supported! Please upload a .pdf, .doc, .docx, .pub or .ppt file')
            return attachments
        else:
            return attachments

class TeacherCreationForm(forms.ModelForm):
    """A form for creating new teacher. Includes all the required fields"""
    plan = forms.CharField(required=True, max_length=20, widget=forms.HiddenInput(), initial="monthly")

    class Meta:
        model = Teacher
        fields = ['city', 'country', 'school','plan']
