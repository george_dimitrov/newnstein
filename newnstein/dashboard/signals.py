from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.db.models.signals import pre_delete
from django.contrib.sessions.models import Session
from django.dispatch import receiver
from django.conf import settings

#as the logout request doesnt save the user information
@receiver(user_logged_out)
def sig_user_logged_out(sender, user, request, **kwargs):
    request.user_logged_out = user
