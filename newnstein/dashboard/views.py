from django.shortcuts import render
from django import forms
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required, user_passes_test
from reportlab.pdfgen import canvas

from teacher.models import Lecture, Class, StatsPerTopic, StatsPerLecture, StatsPerQuiz, UserPerLecture
from quiz.models import Quiz
from helper.helper import find_topics, default_start_time, time_plus, time_add
from account.models import User
from student.models import Class as StudentClass

import cStringIO as StringIO
from xhtml2pdf import pisa
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.views import login
from django.core.exceptions import ObjectDoesNotExist
from datetime import timedelta


def in_class(view_func):
    def _wrapped_view_func(request, *args, **kwargs):
        user = User.objects.filter(email=request.user)
        print StudentClass.objects.filter(student_email=request.user).count()==0
        if (user[0].user_type == "TE" and Class.objects.filter(teacher_email=request.user, id=kwargs["class_id"]).count()==0) or (user[0].user_type == "ST" and StudentClass.objects.filter(student_email=request.user, class_id=kwargs["class_id"]).count()==0):
            return redirect("/dashboard")
        return view_func(request, *args, **kwargs)
    return _wrapped_view_func

def custom_login(request):
    if request.user.is_authenticated():
        return redirect('/dashboard')
    else:
        return login(request, 'registration/login.html')

@login_required
def dashboard(request):
    user = User.objects.filter(email=request.user).first()
    if not user:
        return redirect('/')
    elif user.user_type == 'TE':
        if Class.objects.filter(teacher_email=user.email).count() > 0:
            _class = Class.objects.filter(teacher_email=user.email)
            return redirect('/%s/' % (_class[0].id))
        else:
            return redirect('/settings/class/add')
    elif user.user_type == 'ST':
        if StudentClass.objects.filter(student_email=user.email).count() > 0:
            _class = StudentClass.objects.filter(student_email=user.email)
            return redirect('/%s/' % (_class[0].class_id))
        else:
            return redirect('/settings/class/view')

#This function deals with calculating the statistics for the topics
#This information will be used to display analytics for teachers
def recalculate_statistics(class_id):
    all_lectures = Lecture.objects.filter(class_id = class_id)
    topics = find_topics(class_id)
    for topic in topics:
        topic_stats = None
        try:
            topic_stats = StatsPerTopic.objects.get(topic =  topic, class_id = class_id)
            topic_stats.duration =  default_start_time()
            topic_stats.visits = 0
        except ObjectDoesNotExist:
            topic_stats = StatsPerTopic(topic = topic, visits =  0, duration = default_start_time(), class_id = class_id)
            topic_stats.save()
        for lecture in all_lectures:
            if topic == lecture.topic:
                lecture_stats = UserPerLecture.objects.filter(lecture_id =  lecture.id)
                lecture_info = calculate_stats_lecture(lecture_stats, lecture, class_id)
                if isinstance(lecture_info['duration'], int):
                    topic_stats.duration = time_plus( topic_stats.duration, timedelta(seconds=lecture_info['duration']))
                else:
                    topic_stats.duration = time_plus(lecture_info['duration'], topic_stats.duration)
                topic_stats.visits += lecture_info['visits']
                topic_stats.save()


#This function deals with calculating statistics for lectures
#It is a helper function for "recalculate_statistics" making it more modular
def calculate_stats_lecture(lectures, lecture_obj, class_id):
    if lectures.exists():
        total_duration = 0
        total_visits = 0
        for lecture in lectures:
            if isinstance(total_duration, int):
                total_duration = time_plus(lecture.duration, timedelta(seconds=total_duration))
            else:
                total_duration = time_plus(total_duration, lecture.duration)
            total_visits += lecture.visits
        try:
            lecture_stats = StatsPerLecture.objects.get(lecture_id =  lecture_obj)
            lecture_stats.duration = total_duration
            lecture_stats.visits = total_visits
            lecture_stats.save()
        except ObjectDoesNotExist:
            new_lecture_stats = StatsPerLecture(lecture_id =  lecture_obj, duration = total_duration, visits = total_visits, class_id = class_id)
            new_lecture_stats.save()
        return {'duration': total_duration, 'visits': total_visits}
    else:
        return {'duration': 0, 'visits': 0}

@login_required
@in_class
def main(request, class_id):
    recalculate_statistics(class_id)
    context = {
        "lecture_data" : Lecture.objects.filter(class_id=class_id),
        "quiz_data" : Quiz.objects.filter(class_id=class_id),
        "topics_stats" : StatsPerTopic.objects.filter(class_id=class_id),
        "lecture_duration" : StatsPerLecture.objects.filter(class_id=class_id).order_by('-duration'),
        "lecture_visits" : StatsPerLecture.objects.filter(class_id=class_id).order_by('-visits'),
        "title": "Dashboard",
    }
    return render(request, "dashboard/main.html", context)

@login_required
@in_class
def view_lecture(request, class_id, id=None):
    lecture_to_display =  Lecture.objects.filter(id=id)[0]
    lecture_topic = Lecture.objects.filter(topic=lecture_to_display.topic).order_by('title')
    prev_lecture_data=None
    next_lecture_data=None

    #If there is only only one lecture at the topic, no need to check for prev or next
    if lecture_topic.count() != 1:
        #Go through each lecture in the topic
        for index, lecture in enumerate(lecture_topic):
            if lecture.title == lecture_to_display.title:
                #if its the first lecture of the topic, return only next
                if index == 0:
                    next_lecture_data=lecture_topic[index+1]
                #if the last one in the topic, return only prev
                elif (index+1) == lecture_topic.count():
                    prev_lecture_data=lecture_topic[index-1]
                #if its in the middle of the request, return prev and next
                elif index != 0:
                    prev_lecture_data=lecture_topic[index-1]
                    next_lecture_data=lecture_topic[index+1]
    context = {
        "lecture_data" : lecture_to_display,
        "prev_lecture_data" : prev_lecture_data,
        "next_lecture_data" : next_lecture_data,
        "title": "View Lecture",
    }
    return render(request, "lecture/view_lecture.html", context)

@login_required
@in_class
def export_lecture(request, class_id, id=None):
    context = {
        "lecture" : Lecture.objects.filter(id=id)[0],
    }

    #the following part of the code was written with the help of the following site: http://stackoverflow.com/questions/1377446/render-html-to-pdf-in-django-site
    #the reason why we are rendering the pdf this way and not the way we render the attachment iss because content is technically HTML code
    #so in this case, I encode the html template (template_pdf.html) using the reportlab and pisa
    #as for the encoding character right now using a standard ISO-8859-1. If this will be generating problems on browsers change it to utf-8
    template = get_template("lecture/template_pdf.html")
    context_pdf = Context(context)
    html  = template.render(context_pdf)
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    else:
        return HttpResponse('We had some errors! Please try again or contant the site administrator')

@login_required
@in_class
def view_topic(request, class_id, topic=None):
    context = {
        "lecture_data" : Lecture.objects.filter(topic=topic, class_id=class_id).order_by('title'),
        "title": "View Topic",
        "topic": topic
    }
    return render(request, "lecture/view_topic.html", context)

@login_required
@in_class
def attachments_lecture(request, class_id, id=None):
    for lecture in Lecture.objects.filter(id=id):
        if lecture.attachments.name.endswith('.pdf'):
            with open(lecture.attachments.name, 'rb') as pdf:
                response = HttpResponse(pdf.read(), content_type='application/pdf')
                response['Content-Disposition'] = 'inline;  filename='+lecture.attachments.name
                return response
        word_extensions = [".doc", ".docx", ".ppt", ".pub"]
        if lecture.attachments.name.endswith(tuple(word_extensions)):
            response = HttpResponse(lecture.attachments, content_type='application/vnd.ms-word')
            response['Content-Disposition'] = 'attachment; filename='+lecture.attachments.name
            return response
        else:
            raise forms.ValidationError(u'File not supported! Please upload a .pdf, .doc, .ppt or .docx file')
