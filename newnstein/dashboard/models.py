from account.models import User
from django.db import models

# Create your models here.
class WebRequest(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User,blank=True,null=True)
    lecture_id =  models.IntegerField(blank=True,null=True)
    quiz_id =  models.IntegerField(blank=True,null=True)
    exercise_id =  models.IntegerField(blank=True,null=True)
