"""newnstein URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""

from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^$', 'dashboard.views.main', name='dashboard'),
    url(r'^teacher/', include('teacher.urls')),
    # url(r'^student/', include('student.urls')),
    url(r'^lecture/view/(?P<id>\d+)/$', 'dashboard.views.view_lecture', name='view_lecture'),
    url(r'^lecture/view/(?P<id>\d+)/export/$', 'dashboard.views.export_lecture', name='export_lecture'),
    url(r'^lecture/view/(?P<id>\d+)/attachments/$', 'dashboard.views.attachments_lecture', name='attachments_lecture'),
    url(r'^topic/view/(?P<topic>[\w -:()]+)/$', 'dashboard.views.view_topic', name='view_topic'),
    url(r'^quizzes/', include('quiz.urls')),
    url(r'^exercises/',include('exercises.urls'))
]
