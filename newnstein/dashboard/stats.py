from teacher.models import Lecture
from quiz.models import Quiz
from exercises.models import AvailableExercise, Exercise
from teacher.models import UserPerLecture, UserPerQuiz, UserPerExercise
from dashboard.models  import WebRequest

from django.utils import timezone
from django.db import models
from helper.helper import default_start_time, time_plus
from django.utils.html import escape
from django.http import HttpResponse

import datetime

def process_time_request(users_request, user_request_info, check_path):
        if users_request is not None:
            if not "login" in check_path:
                request_time = timezone.now()
                request_time = user_request_info.first().time
                time_since_request = timezone.now() - request_time
                users_request.duration = time_plus(users_request.duration, time_since_request)
                if "quizzes" in check_path and "take" in check_path:
                    users_request.save()
                else:
                    users_request.visits+=1
                    users_request.save()

class StatsMiddleware(object):

    def process_response(self, request, response):
        #If user is not logged in, dont do anything
        if hasattr(request, 'user') and request.user.is_authenticated() or hasattr(request, 'user_logged_out'):
            current_user =  None
            #If logged out, use that username
            if hasattr(request, 'user_logged_out'):
                current_user = request.user_logged_out
            else:
                current_user = request.user

            web_path = request.path
            check_path = web_path.split('/')
            #Get the latest request done by the user
            user_request_info = WebRequest.objects.filter(user = current_user).order_by('-id')
            #process the new request_time
            if user_request_info.exists():
                latest_request = user_request_info.first()
                if latest_request.lecture_id is not None:
                    users_lectures = UserPerLecture.objects.filter(lecture_id = Lecture.objects.filter(id = latest_request.lecture_id).first() , user_id =  current_user).order_by('-id').first()
                    process_time_request(users_lectures, user_request_info, check_path)
                elif latest_request.quiz_id is not None:
                    users_quiz = UserPerQuiz.objects.filter(quiz_id = Quiz.objects.filter(id = latest_request.quiz_id).first() , user_id =  current_user).order_by('-id').first()
                    process_time_request(users_quiz, user_request_info, check_path)
                elif latest_request.exercise_id is not None:
                    users_exercise = UserPerExercise.objects.filter(exercise_id = Exercise.objects.filter(id = latest_request.exercise_id).first() , user_id =  current_user).order_by('-id').first()
                    process_time_request(users_exercise, user_request_info, check_path)
            #delete the processed request
            user_request_info.delete()

            if "lecture" in check_path and "view" in check_path:
                request_lecture_id = check_path[check_path.index("view")+1]
                request_class_id = check_path[1]
                #before processing the new reuqest, find the most recent request done by the user (if there is any)
                #and calculate the new time
                users_lectures = UserPerLecture.objects.filter(lecture_id = Lecture.objects.filter(id = request_lecture_id).first(), class_id = request_class_id, user_id = current_user).order_by('-id')
                #if there is no previous lecture, create a record
                #if yes, calculate the new duration
                if not users_lectures:
                    new_duration_lecture_instance = UserPerLecture(lecture_id =  Lecture.objects.filter(id = request_lecture_id).first(), class_id = request_class_id, user_id =  current_user, duration = default_start_time())
                    new_duration_lecture_instance.save()

                #now that we have process this, save the request
                Lecture_request = WebRequest(time = timezone.now(), user = current_user, lecture_id = request_lecture_id)
                Lecture_request.save()
            elif "quizzes" in check_path and "take" in check_path:
                request_quiz_id = check_path[check_path.index("quizzes")+1]
                request_class_id = check_path[1]
                #before processing the new reuqest, find the most recent request done by the user (if there is any)
                #and calculate the new time
                users_quizzes = UserPerQuiz.objects.filter(quiz_id = Quiz.objects.filter(id = request_quiz_id).first(), class_id = request_class_id, user_id = current_user).order_by('-id')
                #if there is no previous quiz, create a record
                #if yes, calculate the new duration
                if not users_quizzes:
                    new_duration_quiz_instance = UserPerQuiz(quiz_id =  Quiz.objects.filter(id = request_quiz_id).first(), class_id = request_class_id, user_id =  current_user, duration = default_start_time())
                    new_duration_quiz_instance.save()

                #now that we have process this, save the request
                Quiz_request = WebRequest(time = timezone.now(), user = current_user, quiz_id = request_quiz_id)
                Quiz_request.save()
            elif "exercises" in check_path and "view" in check_path:
                request_exercise_id = AvailableExercise.objects.filter(url=check_path[check_path.index("view")+1]).first()
                request_class_id = check_path[1]
                current_exercise = Exercise.objects.filter(exercise_id = request_exercise_id.id).first()
                #before processing the new reuqest, find the most recent request done by the user (if there is any)
                #and calculate the new time
                users_exercises = UserPerExercise.objects.filter(exercise_id = current_exercise, class_id = request_class_id, user_id = current_user).order_by('-id')
                #if there is no previous exercises, create a record
                #if yes, calculate the new duration
                if not users_exercises:
                    new_duration_exercise_instance = UserPerExercise(exercise_id =  current_exercise, class_id = request_class_id, user_id =  current_user, duration = default_start_time())
                    new_duration_exercise_instance.save()

                #now that we have process this, save the request
                exercise_request = WebRequest(time = timezone.now(), user = current_user, exercise_id = current_exercise.id)
                exercise_request.save()
            elif "favicon" not in web_path:
                web_request = WebRequest(time = timezone.now(), user = current_user)
                web_request.save()

        return response
