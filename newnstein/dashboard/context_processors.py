from teacher.models import Lecture, Class
from student.models import Class as StudentClass
from quiz.models import Quiz
from exercises.models import Exercise
from account.models import User
from helper.helper import find_topics, find_categories, find_exercises
import operator

def retrieve_navbar_content(request):
    current_class = False
    if request.user:
        user = User.objects.filter(email=request.user)
        if len(user) == 0:
            return {}

        if user[0].user_type == 'TE':
            classes = Class.objects.filter(teacher_email=request.user)
        else:
            classes = StudentClass.objects.filter(student_email=request.user)

        if hasattr(request, 'resolver_match'):
            class_id = request.resolver_match.kwargs.get('class_id')
            if class_id:
                current_class = Class.objects.filter(id=class_id).first()
                if not len(classes) > 1:
                    classes=None
                exercises = find_exercises(class_id)

            return {
                "nav_list" : Lecture.objects.filter(class_id=class_id).order_by('title'),
                "quiz_list" : Quiz.objects.filter(class_id=class_id).order_by('title'),
                "exercise_list" : Exercise.objects.filter(class_id=class_id),
                "topics" : sorted(find_topics(class_id), key=lambda s: s.lower()),
                "exercises": find_exercises(class_id),
                "user_type": user[0].user_type,
                "categories": sorted(find_categories(), key=lambda s: s.lower()),
                "class_id": class_id,
                "classes": classes,
                "current_class": current_class
            }

    return {
        "classes": classes,
        "current_class": current_class
    }
