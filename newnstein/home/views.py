from django.shortcuts import render

def home(request):
    title = 'Home'
    context = {
        "title": title
    }
    return render(request, "home/home.html", context)

def login(request):
    title = 'Login'
    context = {
        "title": title
    }
    return render(request, "other/login.html", context)

def error400(request):
    context = {
        error_img : "img/error/400.png",
        title: "400",
        error_msg : "Bad Request"
    }
    return render(request,'error/error.html', context)

def error403(request):
    context = {
        error_img : "img/error/403.png",
        title: "403",
        error_msg : "Access Denied"
    }
    return render(request,'error/error.html', context)

def error404(request):
    context = {
        error_img : "img/error/404.png",
        title: "404",
        error_msg : "Page Not Found"
    }
    return render(request,'error/error.html', context)

def error500(request):
    context = {
        error_img : "img/error/500.png",
        title: "500",
        error_msg : "Internal Server Error"
    }
    return render(request,'error/error.html', context)
